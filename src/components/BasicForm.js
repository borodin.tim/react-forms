import validator from 'email-validator';

import useInput2 from '../hooks/use-input2';

const BasicForm = () => {
  const {
    value: firstName,
    isValid: firstNameIsValid,
    hasError: firstNameHasError,
    valueChangeHandler: firstNameChangeHandler,
    inputBlurHandler: firstNameBlurHandler,
    resetStates: firstNameResetStates
  } = useInput2(value => value.length > 0);

  const {
    value: lastName,
    isValid: lastNameIsValid,
    hasError: lastNameHasError,
    valueChangeHandler: lastNameChangeHandler,
    inputBlurHandler: lastNameBlurHandler,
    resetStates: lastNameResetStates
  } = useInput2(value => value.length > 0);

  const {
    value: email,
    isValid: emailIsValid,
    hasError: emailHasError,
    valueChangeHandler: emailChangeHandler,
    inputBlurHandler: emailBlurHandler,
    resetStates: emailResetStates
  } = useInput2(value => validator.validate(value));

  let formIsValid = false;
  if (firstNameIsValid && lastNameIsValid && emailIsValid) {
    formIsValid = true;
  }

  const handleFormSubmit = (event) => {
    event.preventDefault();

    if (!formIsValid) {
      return;
    }

    console.log('firstName:', firstName);
    console.log('lastName:', lastName);
    console.log('email:', email);

    firstNameResetStates();
    lastNameResetStates();
    emailResetStates();
  }

  const firstNameClass = firstNameHasError ? 'form-control invalid' : 'form-control';
  const lastNameClass = lastNameHasError ? 'form-control invalid' : 'form-control';
  const emailClass = emailHasError ? 'form-control invalid' : 'form-control';

  return (
    <form onSubmit={handleFormSubmit}>
      <div className='control-group'>
        <div className={firstNameClass}>
          <label htmlFor='firstname'>First Name</label>
          <input
            autoComplete="off"
            type='text'
            id='firstname'
            value={firstName}
            onChange={firstNameChangeHandler}
            onBlur={firstNameBlurHandler}
          />
          {firstNameHasError && <p className="error-text">First Name must not be empty</p>}
        </div>
        <div className={lastNameClass}>
          <label htmlFor='lastname'>Last Name</label>
          <input
            autoComplete="off"
            type='text'
            id='lastname'
            value={lastName}
            onChange={lastNameChangeHandler}
            onBlur={lastNameBlurHandler}
          />
          {lastNameHasError && <p className="error-text">Last Name must not be empty</p>}
        </div>
      </div>
      <div className={emailClass}>
        <label htmlFor='email'>E-Mail Address</label>
        <input
          autoComplete="off"
          type='email'
          id='email'
          value={email}
          onChange={emailChangeHandler}
          onBlur={emailBlurHandler}
        />
        {emailHasError && <p className="error-text">Email must be valid</p>}
      </div>
      <div className='form-actions'>
        <button disabled={!formIsValid}>Submit</button>
      </div>
    </form>
  );
};

export default BasicForm;
