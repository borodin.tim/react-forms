import { useReducer } from 'react';

const initialInputState = {
    value: '',
    isTouched: false
};

const inputStateReducer = (state, action) => {
    if (action.type === 'INPUT') {
        return {
            ...state,
            value: action.val
        }
    }
    if (action.type === 'BLUR') {
        return {
            ...state,
            isTouched: true
        };
    }
    if (action.type === 'RESET') {
        return {
            value: '',
            isTouched: false
        };
    }

    return initialInputState;
}

const useInputReducer = (validateValue) => {
    const [inputState, dispatch] = useReducer(inputStateReducer, initialInputState);

    const valueIsValid = validateValue(inputState.value);
    const hasError = !valueIsValid && inputState.isTouched;

    const valueChangeHandler = (event) => {
        dispatch({ type: 'INPUT', val: event.target.value });
    }

    const inputBlurHandler = (event) => {
        dispatch({ type: 'BLUR' });
    }

    const resetStates = () => {
        dispatch({ type: 'RESET' });
    }

    return {
        value: inputState.value,
        isValid: valueIsValid,
        hasError,
        valueChangeHandler,
        inputBlurHandler,
        resetStates
    };
}

export default useInputReducer;