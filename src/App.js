// import SimpleInput from './components/SimpleInput';
// import BasicForm from './components/BasicForm';
import BasicFormReducer from './components/BasicFormReducer';

function App() {
  return (
    <div className="app">
      <BasicFormReducer />
    </div>
  );
}

export default App;
